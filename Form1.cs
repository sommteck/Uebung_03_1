using System;
using System.Windows.Forms;
using System.Drawing;


namespace Uebung_03__CLI_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jetzt kommt ein Fenster.");
            Console.WriteLine("Bitte eine Taste drücken!");
            Console.ReadKey();
            Form eineForm = new Form();
            Application.Run(eineForm);
            Console.ReadKey();

            CForm ersteForm = new CForm();
            Application.Run(ersteForm);
            Console.ReadKey();

        }
    }

    class CForm : Form
    {
        public CForm()
        {
            this.Text = "Fenstertext";
            this.BackColor = Color.Blue;
            this.ControlBox = true;
            this.ForeColor = Color.Wheat;
        }
    }
}
